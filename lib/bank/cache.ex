defmodule Bank.Cache do
  use GenServer

  def start_link initial_balance do
    GenServer.start_link(__MODULE__, initial_balance)
  end

  def init(initial_balance) do
    {:ok, initial_balance}
  end

  def get_balance(pid) do
    GenServer.call pid, :get
  end

  def save_balance(new_balance, pid) do
    GenServer.cast(pid,{:save, new_balance})
  end

  def handle_call(:get, _from, balance) do
    {:reply, balance, balance}
  end

  def handle_cast({:save, new_balance}, _balance) do
    {:noreply, new_balance}
  end
end
