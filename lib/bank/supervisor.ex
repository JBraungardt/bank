defmodule Bank.Supervisor do
  use Supervisor

  def start_link(initial_balance) do
    bank_supervisor = {:ok, sup} = Supervisor.start_link(__MODULE__, [initial_balance])
    start_children(sup, initial_balance)
    bank_supervisor
  end


  def start_children(sup, initial_balance) do
		{:ok, cache_pid} = 
      Supervisor.start_child(sup, worker(Bank.Cache, [initial_balance]))
    Supervisor.start_child(sup, worker(Bank, [cache_pid]))
  end

  def init(_) do
    supervise([], strategy: :rest_for_one)
  end


end
